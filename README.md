## Paragraph Handler

Paragraph handler allows you to provide "Paragraph Handler" classes
that can handle pre-processing and altering the render arrays of
Paragraph entities when they are displayed.

This helps to prevent .theme files from getting unruly and allows you
to make use of traits and inheritance for your Paragraph display logic.

This module is only useful for developers.

### Requirements

Requires the Paragraphs module:
https://www.drupal.org/project/paragraphs

### Install

Install module like any other contributed module or via composer require:

```bash
composer require drupal/paragraph_handler
```

### Usage

Create ParagraphHandler plugins to govern display logic for your paragraphs.

See _Drupal\paragraph_handler\Plugin\ParagraphHandlerInterface_.

### Example

```php
namespace Drupal\mymodule\Plugin\ParagraphHandler;

use Drupal\paragraph_handler\Plugin\ParagraphHandlerBase;
use Drupal\Core\Url;

/**
 * Class Card.
 *
 * @ParagraphHandler(
 *   id = "card",
 *   label = @Translation("Card")
 * )
 */
class Card extends ParagraphHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, array $element) {
    if ($this->isNested()) {
      $variables['nested_class'] = TRUE;
    }
    $variables['new_variable'] = t('Test');
  }

  /**
   * {@inheritdoc}
   */
  public function build(array &$build) {
    $build['link'] = [
      '#type' => 'link',
      '#title' => t('Link title'),
      '#text' => t('Link text'),
      '#url' => Url::fromUri('<front>'),
    ];
  }
}
```

Place your paragraph handler under src\Plugin\ParagraphHandler\ folder
inside your module.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
