<?php

namespace Drupal\paragraph_handler\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Base class for Paragraph handler plugins.
 */
abstract class ParagraphHandlerBase extends PluginBase implements ParagraphHandlerInterface {

  use StringTranslationTrait;

  /**
   * The Paragraph being handled.
   *
   * @var \Drupal\paragraphs\Entity\ParagraphInterface
   */
  protected $paragraph;

  /**
   * The first parent of this paragraph.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected $parentEntity;

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, array $element) {
  }

  /**
   * {@inheritdoc}
   */
  public function build(array &$build) {
  }

  /**
   * Initialize the plugin by providing a paragraph.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   A Paragraph entity.
   *
   * @return $this
   */
  public function init(ParagraphInterface $paragraph) {
    $this->paragraph = $paragraph;
    // Set parent entity.
    $this->parentEntity = $paragraph->getParentEntity();
    return $this;
  }

  /**
   * Prepare and dispatch the preprocess method.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   A paragraph being preprocessed.
   * @param array $variables
   *   A set of variables from the theme layer.
   */
  public function dispatchPreprocess(ParagraphInterface $paragraph, array &$variables) {
    $this->init($paragraph);
    $element = $this->getRenderable($variables);
    if (method_exists($this, 'preprocess')) {
      $this->preprocess($variables, $element);
    }
  }

  /**
   * Get the renderable element.
   *
   * @param array $variables
   *   Variables from the theme layer.
   *
   * @return array
   *   A render array, or an empty array.
   */
  public function getRenderable(array $variables) {
    $name = $this->getRenderElementName();
    return $variables['elements'][$name] ?? [];
  }

  /**
   * Get the name of the element that holds this paragraph type's render array.
   *
   * @return mixed|string
   *   The name of this paragraph type's render element.
   */
  public function getRenderElementName() {
    return $this->paragraph->bundle();
  }

  /**
   * Determine whether this paragraph is a child of another paragraph.
   *
   * @return bool
   *   Whether paragraph is a child of another paragraph.
   */
  public function isNested() {
    return $this->parentEntity->getEntityTypeId() !== 'node';
  }

  /**
   * Prepare and dispatch the build method.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   A paragraph being rendered.
   * @param array $build
   *   A build array for a paragraph entity.
   */
  public function dispatchBuild(ParagraphInterface $paragraph, array &$build) {
    $this->init($paragraph);
    if (method_exists($this, 'build')) {
      $this->build($build);
    }
  }

}
